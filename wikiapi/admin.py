from django.contrib import admin
from reversion.admin import VersionAdmin
from .models import Page

@admin.register(Page)
class PageAdmin(VersionAdmin):
    pass
