from rest_framework import serializers
from .models import Page

class PageListSerializer(serializers.ModelSerializer):
    uri = serializers.HyperlinkedIdentityField(view_name='page_detail')
    class Meta:
        model = Page
        fields = [
            'title',
            'text',
            'uri'
        ]
class PageDetailSerializer(serializers.ModelSerializer):
    history = serializers.HyperlinkedIdentityField(view_name='page_history')
    class Meta:
        model = Page
        fields = [
            'title',
            'text',
            'history'
        ]

class PageHistorySerializer(serializers.Serializer):
    title = serializers.CharField()
    text = serializers.CharField()
    uri = serializers.URLField(required=False)

class PageHistorySmallSerializer(serializers.Serializer):
    title = serializers.CharField()
    text = serializers.CharField()