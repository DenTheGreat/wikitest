from django.http import Http404
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from reversion.models import Version
from .serializers import (PageDetailSerializer, 
                          Page, 
                          PageListSerializer, 
                          PageHistorySerializer,
                          PageHistorySmallSerializer)

class PageApiListView(ListCreateAPIView):
    '''
    Список имеющихся страниц + добавление новых
    '''
    serializer_class = PageListSerializer
    queryset = Page.objects.all()

class PageApiDetailView(RetrieveUpdateDestroyAPIView):
    '''
    Отображение текущей версии страницы + обновление или удаление
    '''
    serializer_class = PageDetailSerializer
    queryset = Page.objects.all()

class PageHistoryApiListView(APIView):
    '''
    Список всех версий страницы
    '''
    def get(self,request,**kwargs):
        obj = Page.objects.get(id=kwargs.get('pk'))
        versions = Version.objects.get_for_object(obj)
        history_objects = []
        for i in range(len(versions)):
            dict = versions[i].field_dict
            history_objects.append({
                "title": dict['title'],
                "text" : dict['text'],
                "uri" : request.build_absolute_uri() + "{id}/".format(id=versions[i].id),
            })
        return Response(PageHistorySerializer(history_objects,many=True,context={'request': request}).data)

class PageHistoryApiDetailView(APIView):
    '''
    Отображение конкретной версии страницы из истории
    + Возвращение выбраной версии в статус текущей пустым PATCH запросом
    '''
    def get(self,request,**kwargs):
        version = self.validate(kwargs)
        return Response(PageHistorySmallSerializer(version.field_dict,context={'request': request}).data)
    def patch(self, request, **kwargs):
        version = self.validate(kwargs)
        version.revert()
        return Response({"done":True})
    def validate(self,kwargs):
        obj = Page.objects.get(id=kwargs.get('obj_pk'))
        try:
            versions = Version.objects.get_for_object(obj)
            version = Version.objects.get(id=kwargs.get('his_pk'))
        except:
            raise Http404("History doesn't exist")
        if not version in versions:
            raise Http404("History doesn't belong to this object")
        return version