"""wikitest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from wikiapi.views import PageApiListView, PageApiDetailView, PageHistoryApiListView, PageHistoryApiDetailView

urlpatterns = [
    re_path(r'^$', PageApiListView.as_view(),name='page_list'),
    re_path(r'^(?P<pk>\d+)/$', PageApiDetailView.as_view(), name='page_detail'),
    re_path(r'^(?P<pk>\d+)/history/$', PageHistoryApiListView.as_view(), name='page_history'),
    re_path(r'^(?P<obj_pk>\d+)/history/(?P<his_pk>\d+)/$', PageHistoryApiDetailView.as_view(), name='page_history_detail'),
    path('admin/', admin.site.urls),
]
